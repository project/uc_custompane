<?php
function uc_custompane_form() {
  $form['custompane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Pane Title'),
    '#description' => t('Change the title for the custom pane.'),
    '#default_value' => variable_get('custompane_title', CUSTOMPANE_TITLE)
  );

  $form['custompane_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom Pane Description'),
    '#description' => t('Change the description for the custom pane.'),
    '#default_value' => variable_get('custompane_desc', CUSTOMPANE_DESC)
  );

  $form['custompane_form'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom Pane Form'),
    '#description' => t('Enter the PHP code that is used for this form.  Don\'t worry about the #default_value, #title, and #description.'),
    '#default_value' => variable_get('custompane_form', uc_custompane_get_form())
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 30,
  );

  $form['#submit'][] = 'uc_custompane_submit';

  return $form;
}

function uc_custompane_submit( $form, &$form_state ) {
  variable_set('custompane_title', $form_state['values']['custompane_title']);
  variable_set('custompane_desc', $form_state['values']['custompane_desc']);
  variable_set('custompane_form', $form_state['values']['custompane_form']);
}
